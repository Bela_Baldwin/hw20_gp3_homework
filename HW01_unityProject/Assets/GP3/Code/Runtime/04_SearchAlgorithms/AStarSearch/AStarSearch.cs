﻿using System.Collections.Generic;
using System.Linq;

namespace GP3._04_SearchAlgorithms.AStarSearch
{
    public class AStarSearch : SearchBase
    {
        protected override void InitializeSearch()
        {
        	_startNode = _startMarker.ClosestGridNode;
        	_goalNode = _endMarker.ClosestGridNode;

        	foreach (GridNode gridNode in _visited.Keys)
        	{
        		gridNode.Reset();
        	}
        	
        	_openList = new List<GridNode>();
        	_visited = new Dictionary<GridNode, GridNode>();

        	_openList.Add(_startNode);
        }

        protected override bool StepToGoal()
        {
	        // sort all with heuristic function f(n) = (g)n + h(n)
        	_openList = _openList.OrderBy(n => n.CostSoFar + n.Heuristic).ToList();
        	GridNode current = _openList[0];
        	
        	// goal found
        	if (current == _goalNode)
        	{
        		return true;
        	}

        	foreach (GridNode next in current.Neighbours)
        	{
        		if (next.IsWall)
        		{
        			continue;
        		}
                //Dijkstra code - calculate cost of nodes
                float newCost = current.CostSoFar + next.Cost;
                bool alreadyVisited = _visited.ContainsKey(next);
                if (alreadyVisited)
                {
	                if (newCost < next.CostSoFar)
	                {
		                next.CostSoFar = newCost;
		                _visited[next] = current;
		                _openList.Add(next);
		                next.SetGridNodeSearchState(GridNodeSearchState.Queue);
	                }
                }
                
        		if (!_visited.ContainsKey(next))
        		{
        			next.Heuristic = GetHeuristic(_goalNode, next);
        			
        			_openList.Add(next);
        			_visited.Add(next, current);
                    //Dijkstra code - add cost to 
                    next.CostSoFar = newCost;
                    
        			next.SetGridNodeSearchState(GridNodeSearchState.Queue);
                }
        	}

        	_openList.Remove(current);
        	current.SetGridNodeSearchState(GridNodeSearchState.Processed);
        	// not yet finished
        	return false;
        }

        private float GetHeuristic(GridNode goal, GridNode next)
        {
        	return (goal.transform.position - next.transform.position).magnitude;
        }
    }
}