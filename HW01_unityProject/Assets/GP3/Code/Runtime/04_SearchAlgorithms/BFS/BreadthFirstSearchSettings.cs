﻿using GP3.Core;
using UnityEngine;
using UnityEngine.Serialization;

namespace GP3._04_SearchAlgorithms.BFS
{
	[CreateAssetMenu(fileName = "BreadthFirstSearchSettings", menuName = "SearchAlgorithms/New BreadthFirstSearchSettings", order = 200)]
	public class BreadthFirstSearchSettings : ScriptableSingleton<BreadthFirstSearchSettings>
	{
		[FormerlySerializedAs("_normalNodeColor")]
		[Header("Colors for Search States")]
		[SerializeField] private Color _noneNodeColor;
		[FormerlySerializedAs("_QueueNodeColor")] [SerializeField] private Color _queueNodeColor;
		[SerializeField] private Color _processedNodeColor;
		[SerializeField] private Color _pathNodeColor;
		[Header("Colors for Node Types")]
		[SerializeField] private Color _groundNodeColor;
		[SerializeField] private Color _wallNodeColor;
		[SerializeField] private Color _waterNodeColor;
		[SerializeField] private Color _swampNodeColor;

		public Color NoneNodeColor => _noneNodeColor;
		public Color WallNodeColor => _wallNodeColor;
		public Color WaterNodeColor => _waterNodeColor;
		public Color SwampNodeColor => _swampNodeColor;
		public Color GroundNodeColor => _groundNodeColor;
		public Color QueueNodeColor => _queueNodeColor;
		public Color ProcessedNodeColor => _processedNodeColor;
		public Color PathNodeColor => _pathNodeColor;

		[Header("Cost for Grid Types")]
		[SerializeField] private float _costGroundNode;
		[SerializeField] private float _costWallNode;
		[SerializeField] private float _costWaterNode;
		[SerializeField] private float _costSwampNode;

		public float CostGroundNode => _costGroundNode;
		public float CostWallNode => _costWallNode;
		public float CostWaterNode => _costWaterNode;
		public float CostSwampNode => _costSwampNode;

	}
}