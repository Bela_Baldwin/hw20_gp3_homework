﻿using System;
using UnityEngine;

namespace UEGP3.FSM.Quests
{
	public class QuestItem : MonoBehaviour
	{
		public bool IsCollected => _isCollected;
		private bool _isCollected;
		[Header("Quest Item Specific Settings")]
		[Tooltip("The time the player has to return the quest item the NPC, otherwise the quest fails.")]
		[SerializeField] private float _questTimer;
		private bool _isActive;
		private float _timerStartValue;
		public bool IsFailed => _isFailed;
		private bool _isFailed;

		private void Awake()
		{
			gameObject.SetActive(false);
			_timerStartValue = _questTimer;
		}

		private void Update()
		{
			transform.position = new Vector3(transform.position.x, transform.position.y * Mathf.Sin(Time.time), transform.position.z);
			transform.rotation = Quaternion.Euler(new Vector3(0, 90 * Time.deltaTime, 0));

			// Counts down the timer, when the QuestItem is active
			if (_isActive)
			{
				if (_questTimer > 0)
				{
					_questTimer -= Time.deltaTime;
				}
				// Sets required states to switch quest status, when time runs out
				else
				{
					_questTimer = _timerStartValue;
					_isFailed = true;
					_isActive = false;
					gameObject.SetActive(false);
				}
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			if (other.CompareTag("Player"))
			{
				Collect();
			}
		}
		
		private void Collect()
		{
			_isCollected = true;
			gameObject.SetActive(false);
			//Stops timer & resets it
			_isActive = false;
			_questTimer = _timerStartValue;
		}

		public void Activate()
		{
			gameObject.SetActive(true);
			//Starts timer
			_isActive = true;
		}
	}
}