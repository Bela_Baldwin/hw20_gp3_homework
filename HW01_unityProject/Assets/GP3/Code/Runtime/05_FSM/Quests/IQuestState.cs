﻿using UnityEngine;

namespace UEGP3.FSM.Quests
{
	public interface IQuestState
	{
		IQuestState Execute(QuestNPC npc);
		void Enter(QuestNPC npc);
		void Exit(QuestNPC npc);
	}
	
	public class QuestAvailableState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			if (Input.GetMouseButtonDown(0) && npc.IsPlayerClose)
			{
				return QuestNPC.QuestActiveState;
			}
			
			return QuestNPC.QuestAvailableState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.DisplayQuestIcon(npc.AvailableStateMesh);
			npc.SetNPCAnswer("Hey you! Go get that item for me, would ya?");
		}

		public void Exit(QuestNPC npc)
		{
			npc.DisplayQuestIcon(null);
			npc.SetNPCAnswer("");
		}
	}
	
	public class QuestActiveState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			if (npc.RequirementsMet)
			{
				return QuestNPC.QuestTaskDoneState;
			}
			// Now returns a new state if the quest is failed, because the QuestItem timer has reached 0
			if (npc.QuestFailed)
			{
				return QuestNPC.QuestFailedState;
			}

			return QuestNPC.QuestActiveState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.QuestItem.Activate();
			npc.SetNPCAnswer("Remember to bring me my item please");
		}

		public void Exit(QuestNPC npc)
		{
			npc.SetNPCAnswer("");
		}
	}
	
	public class QuestTaskDoneState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			if (npc.IsPlayerClose && Input.GetMouseButtonDown(0))
			{
				return QuestNPC.QuestDoneState;
			}

			return QuestNPC.QuestTaskDoneState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.DisplayQuestIcon(npc.QuestTaskDoneStateMesh);
			npc.SetNPCAnswer("Ay what a great finding! Myyyy precious");
		}

		public void Exit(QuestNPC npc)
		{
			npc.DisplayQuestIcon(null);
			npc.SetNPCAnswer("");
		}
	}
	
	public class QuestDoneState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			return QuestNPC.QuestDoneState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.SetNPCAnswer("My precious..");
		}

		public void Exit(QuestNPC npc)
		{
		}
	}
	
	// Added a new state, which simply alters the NPC's bark
	public class QuestFailedState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			return QuestNPC.QuestFailedState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.SetNPCAnswer("Next time be quicker, you lazy git!");
		}

		public void Exit(QuestNPC npc)
		{
		}
	}
}